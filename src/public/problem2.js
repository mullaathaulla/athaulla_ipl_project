let csvToJson = require("convert-csv-to-json");

//let arrayCount = {};

let jsonMatches = csvToJson
  .fieldDelimiter(",")
  .formatValueByType()
  .getJsonFromCsv("/home/athaulla/Desktop/IPL_Project/src/data/matches.csv");

let filteredArray1 = jsonMatches.filter((value) => value.result === "normal");

let arr = [];
filteredArray1.forEach((item, index, array) => {
  if (!arr.includes(item.season)) {
    arr.push(item.season);
  }
});

//console.log(arr);

let finalObj = {};
arr.forEach((outerItem, outerIndex, outerArray) => {
  let filteredArray2 = jsonMatches.filter(
    (value) => value.season === outerArray[outerIndex]
  );
  let newObj = {};
  filteredArray2.forEach((innerItem, innerIndex, innerArray) => {
    let x = innerItem.season;
    if (innerItem.winner in newObj) {
      newObj[innerItem.winner] += 1;
    } else {
      newObj[innerItem.winner] = 1;
    }
  });
  finalObj[outerArray[outerIndex]] = newObj;
});

//console.log(finalObj);

const fs = require("fs");

const data = JSON.stringify(finalObj);

try {
  fs.writeFileSync("src/public/output/matchesWonPerTeamPerYear.json", data);
  console.log("JSON data is saved.");
} catch (error) {
  console.error(err);
}
