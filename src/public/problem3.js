let csvToJson = require("convert-csv-to-json");

let jsonDeliveries = csvToJson
  .fieldDelimiter(",")
  .formatValueByType()
  .getJsonFromCsv("/home/athaulla/Desktop/IPL_Project/src/data/deliveries.csv");

let arrayCount = {};

let jsonMatches = csvToJson
  .fieldDelimiter(",")
  .formatValueByType()
  .getJsonFromCsv("/home/athaulla/Desktop/IPL_Project/src/data/matches.csv");

let filteredArray1 = jsonMatches.filter((value) => value.season === 2016);

let filteredIds = filteredArray1.map((value) => value.id);

let filteredArray3 = jsonDeliveries.filter(
  (value) => filteredIds.includes(value.match_id) && value.inning === 1
);

filteredArray3.map((item, index, array) => {
  if (item.batting_team in arrayCount) {
    arrayCount[item.batting_team] += item.extra_runs;
  } else {
    arrayCount[item.batting_team] = item.extra_runs;
  }
});

// console.log(filteredArray3);
// console.log(arrayCount);

const fs = require("fs");

const data = JSON.stringify(arrayCount);

try {
  fs.writeFileSync(
    "src/public/output/extraRunsConcededPerTeamIn2016.json",
    data
  );
  console.log("JSON data is saved.");
} catch (error) {
  console.error(err);
}
