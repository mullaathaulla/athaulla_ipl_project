let csvToJson = require("convert-csv-to-json");

let arrayCount = {};

let jsonMatches = csvToJson
  .fieldDelimiter(",")
  .formatValueByType()
  .getJsonFromCsv("/home/athaulla/Desktop/IPL_Project/src/data/matches.csv");


jsonMatches.forEach((item,index,array) => {
  if(item.season in arrayCount){
    arrayCount[item.season]+=1;
  }
  else{
    arrayCount[item.season]=1;
  }
});

//console.log(arrayCount);

const fs = require("fs");

const data = JSON.stringify(arrayCount);

try {
  fs.writeFileSync("src/public/output/matchesPerYear.json", data);
  console.log("JSON data is saved.");
} catch (error) {
  console.error(err);
}
