let csvToJson = require("convert-csv-to-json");

let jsonDeliveries = csvToJson
  .fieldDelimiter(",")
  .formatValueByType()
  .getJsonFromCsv("/home/athaulla/Desktop/IPL_Project/src/data/deliveries.csv");

let jsonMatches = csvToJson
  .fieldDelimiter(",")
  .formatValueByType()
  .getJsonFromCsv("/home/athaulla/Desktop/IPL_Project/src/data/matches.csv");

let filteredArray1 = jsonMatches.filter((value) => value.season === 2015);

let filteredIds = filteredArray1.map((value) => value.id);

let filteredArray3 = jsonDeliveries.filter((value) =>
  filteredIds.includes(value.match_id)
);

let obj = {};

filteredArray3.map((item, index, array) => {
  if (item.bowler in obj) {
    obj[item.bowler].noOfRuns += item.total_runs - item.legbye_runs;
    obj[item.bowler].noOfBalls += 1;
  } else {
    obj[item.bowler] = {};
    obj[item.bowler].noOfRuns = item.total_runs - item.legbye_runs;
    obj[item.bowler].noOfBalls = 1;
  }
});

//console.log(obj);

let arr = Object.entries(obj);

let newArray = [];

arr.map((item, index, array) => {
  item.economy = ((item[1].noOfRuns / item[1].noOfBalls) * 6).toFixed(2);
  let tempObj = {};
  tempObj.bowler_name = item[0];
  tempObj.economy = item.economy;
  newArray.push(tempObj);
});

newArray.sort((a, b) => a.economy - b.economy);

let topTenEconomyBowlers = newArray.slice(0, 10);

//console.log(topTenEconomyBowlers);

const fs = require("fs");

const data = JSON.stringify(topTenEconomyBowlers);

try {
  fs.writeFileSync(
    "src/public/output/topTenEconomyBowlersIn2015.json",
    data
  );
  console.log("JSON data is saved.");
} catch (error) {
  console.error(err);
}
